const env = require('./env')

export default {
  // target: 'static',
  generate: {
    fallback: true
  },
  buildDir: 'halohalonews',
  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'Article',
          path: '/article/:id?',
          component: resolve(__dirname, 'pages/Article/index.vue')
        },
        {
          name: 'ArticleList',
          path: '/articleList/:id?',
          component: resolve(__dirname, 'pages/ArticleList/index.vue')
        }
      )
    },
    scrollBehavior: function (to, from, savedPosition) {
      return {x: 0, y: 0}
    }
  },
  env: {
    NUXT_ENV: env[process.env.MODE]
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'HalohaloNEWS',
    htmlAttrs: {
      lang: 'zh-TW'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, viewport-fit=cover"'},
      {
        name: 'description',
        content: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium'
      },
      {
        name: 'keywords',
        content: 'games, video games, finance, news, trend, entertainment, animation, sports, lottery, gambling, betting, entertainer, NFT'
      },
      {hid: 'og:title', property: 'og:title', content: 'HalohaloNEWS'},
      {hid: 'og:url', property: 'og:url', content: 'https://halohalonews.netlify.app/'},
      {
        hid: 'og:description',
        property: 'og:description',
        content: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://halohalonews.netlify.app/icon-200.png'
      },
      {name: 'format-detection', content: 'telephone=no'},
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary'
      }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: 'https://halohalonews.netlify.app/favicon.png'}
    ],
    script: [
      {src: 'https://www.googletagmanager.com/gtag/js?id=G-BZ0Z7V6328'} // gtag
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  tailwindcss: {
    cssPath: '~/assets/css/style.css',
    configPath: 'tailwind.config.js',
    exposeConfig: false,
    config: {},
    injectPosition: 0,
    viewer: true
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~/plugins/vuex-persist', ssr: false},
    {src: '~/plugins/i18n.js', ssr: false},
    {src: '~/plugins/vue-swiper.js', ssr: false},
    {src: '~/plugins/apiService.js', ssr: false},
    {src: '~/plugins/gtag.js', ssr: false},
    {src: '~/plugins/track-code.js', ssr: false},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/composition-api/module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/tailwindcss', '@nuxtjs/i18n', '@nuxtjs/axios'],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vueLoader: {
      transformAssetUrls: {
        video: 'src',
        source: 'src'
      }
    }
  },
  i18n: {
    defaultLocale: 'en',
    langDir: '~/locales/',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      redirectOn: 'root' // recommended
    },
    locales: [
      {code: 'en', iso: 'en', file: 'en.json'},
      {code: 'ja', iso: 'ja', file: 'ja.json'}
    ]
  }
  // server: {
  //   port: 3001 ,// default: 3000
  //   host: '0.0.0.0' // default: localhost
  // }
}
