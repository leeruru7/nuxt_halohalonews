import MD5 from "crypto-js/md5";

const ts = new Date().getTime();
const HASH = MD5(ts + process.env.NUXT_ENV.PRIVATE_KEY + process.env.NUXT_ENV.PUBLIC_KEY);
export const state = () => ({
  isEncryption: true,
  showLog: process.env.NUXT_ENV.LOG,
  apiUrl: `${process.env.NUXT_ENV.API_URL}`,
  parameter: `?ts=${ts}&apikey=${process.env.NUXT_ENV.PUBLIC_KEY}&hash=${HASH}`,
  // newsIdList: [],
  companyId: Number(process.env.NUXT_ENV.COMPANY_ID),
  country: 'us',
  countryCode: '608' // 608 ph , 392 jp
})

export const mutations = {
  setCountry(state, data) {
    state.country = data;
  },
  setCountryCode(state, data) {
    state.countryCode = data;
  },
  // setNewsIdList(state, data) {
  //   state.newsIdList = data;
  // }
}

export const actions = {
  // nuxtServerInit ({commit}) {
  //
  // }
}


