export function parseTime(time, cFormat) {
    if ((typeof time === 'string')) {
        if ((/^[0-9]+$/.test(time))) {
            time = parseInt(time);
        } else {
            // 刪掉時區
            time = time.replace(new RegExp(/\+0000 UTC/gm), '')
            // - 替換成 /
            time = time.replace(new RegExp(/-/gm), '/');
        }
    }

    const date = new Date(time);

    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
    };

    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'

    return format.replace(/{([ymdhi])+}/g, (result, key) => {
        const value = formatObj[key]
        return value.toString().padStart(2, '0')
    })
}
