export default ({i18n, app}) => {
  i18n.defaultLocale = localStorage.locale || 'en';
  i18n.locale = localStorage.locale || 'en';
  app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
    i18n.locale = localStorage.locale;
  }
}
