import CryptoJS from 'crypto-js';

export const service = (axios,store) => ({
  iv() {
    return '0000000000000000';
  },
  aesKey() {
    return process.env.NUXT_ENV.API_KEY;
  },
  apiUrl() {
    return process.env.NUXT_ENV.API_URL;
  },
  companyId() {
    return process.env.NUXT_ENV.COMPANY_ID;
  },
  countryCode() {
    return store.state.countryCode;
  },

  encryptedData(body) {
    const key = CryptoJS.enc.Utf8.parse(this.aesKey());
    const ivKey = CryptoJS.enc.Utf8.parse(this.iv());
    if (body instanceof Object) {
      body = JSON.stringify(body)
    }

    return CryptoJS.AES.encrypt(body, key, {
      iv: ivKey,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }).toString();
  },

  decryptedData(body) {
    const key = CryptoJS.enc.Utf8.parse(this.aesKey());
    const ivKey = CryptoJS.enc.Utf8.parse(this.iv());
    const decrypt = CryptoJS.AES.decrypt(body, key, {
      iv: ivKey,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });

    return CryptoJS.enc.Utf8.stringify(decrypt).toString();
  },

  // 首頁banner
  getBanner(type) {
    const api = this.apiUrl() + 'news/getbanner';
    const data = {
      'company_id': this.companyId(),
      'belongs': 0,
      'game_id': 0,
      'type': type,
      'country_code': this.countryCode()
    };
    return this.post('getBanner', data, api);
  },

  // 文章列表
  newsGet(type, sub_type) {
    const api = this.apiUrl() + 'news/get';
    const data = {
      'company_id': this.companyId(),
      'news_type': type, // 0
      "sub_type": sub_type || 0, // 0=Financial 1=Games/Anime 2=Entertainment 3=Betting Scoop
      'belongs': 0,
      'game_id': 0,
      'country_code': this.countryCode()
    };
    return this.post('newsGet', data, api);
  },

  // 文章內容
  newsContent(id,$store) {
    const api = this.apiUrl() + 'news/getbyid';
    const data = {
      'company_id': this.companyId(),
      'news_id': id,
      'country_code': $store.countryCode
    };
    return this.post('newsContent', data, api,$store);
  },

  post(command, data = {}, api,$store) {
    let _store = $store || store;
    let body = '';
    if (_store.state.isEncryption) {
      body = {company_id: this.companyId(), data: this.encryptedData(data)};
    } else {
      body = {company_id: this.companyId(), data: data};
    }

    if (_store.state.showLog) {
      console.log('request command = ' + command);
      console.log(data);
    }

    return new Promise((resolve, reject) => {
      axios.post(api, JSON.stringify(body), {timeout: 70000}).then(res => {
        if (_store.state.showLog) {
          console.log('$http post 成功 回傳如下');
          console.log('response command = ' + command);
          console.log(res);
        }

        let resData = '';
        if (res.data.error_code === 0) {
          if (res.data.data !== null) {
            resData = _store.state.isEncryption ? JSON.parse(this.decryptedData(res.data.data)) : res.data.data;
          } else {
            resData = '';
          }
          resolve(resData);
        } else {
          reject(res.data);
        }

      }).catch(error => {
        if (_store.state.showLog) {
          console.log('$http post 失敗 回傳如下');
          console.log('command = ' + command);
          console.log(error);
        }
      })
    });
  }
})

export default ({$axios,store}, inject) => {
  const api = service($axios,store);
  inject('api', api);
};
