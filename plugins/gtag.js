export default ({env}) => {
  if (env.NUXT_ENV.MODE === 'prod') {
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-BZ0Z7V6328');
  }
}
