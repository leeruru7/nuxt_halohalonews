module.exports = {
  apps: [
    {
      name: 'halohalonews_dev_ruru',
      exec_mode: 'cluster',
      instances: 'max', // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start',
      port: 3011,
    }
  ]
}
