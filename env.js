module.exports = {
  dev: {
    MODE: 'dev',
    BASE_URL: 'https://halohalonews.netlify.app/',
    API_URL: 'https://gateway.marvel.com/v1/public/',
    PRIVATE_KEY: '8bc5a92f14b1cc3fd1dcd6bc47d1dadf1ace136f',
    PUBLIC_KEY: '56182b4d7f9131fc1783021db5783985',
    COMPANY_ID: 3,
    TAG: '',
    LOG: true,
    META_TITLE: 'HalohaloNEWS',
    META_DESCRIPTION: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium.',
    META_KEYWORDS: 'games, video games, finance, news, trend, entertainment, animation, sports, lottery, gambling, betting, entertainer, NFT',
    META_OG_TITLE: 'HalohaloNEWS',
    META_OG_DESCRIPTION: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium.',
    META_OG_IMAGE: 'og.png'
  },
  prod: {
    MODE: 'prod',
    BASE_URL: 'https://halohalonews.netlify.app/',
    API_URL: 'https://gateway.marvel.com/v1/public/',
    PRIVATE_KEY: '8bc5a92f14b1cc3fd1dcd6bc47d1dadf1ace136f',
    PUBLIC_KEY: '56182b4d7f9131fc1783021db5783985',
    COMPANY_ID: 3,
    TAG: '',
    LOG: false,
    META_TITLE: 'HalohaloNEWS',
    META_DESCRIPTION: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium.',
    META_KEYWORDS: 'games, video games, finance, news, trend, entertainment, animation, sports, lottery, gambling, betting, entertainer, NFT',
    META_OG_TITLE: 'HalohaloNEWS',
    META_OG_DESCRIPTION: 'Continue to provide a wealth of financial, trending and video gaming information. Provide first-hand interesting news, unlock wealth secrets, and create a unique medium.',
    META_OG_IMAGE: 'og.png'
  }
}
